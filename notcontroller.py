class NotController(object):
    def first_action(self, params):
        with open('output.txt', 'w') as f:
            f.write(params)
        return "done"

    def second_action(self, params):
        with open('output.txt', 'r') as f:
            return f.read()

    def third_action(self, params):
        return "someText"

    def error_action(self, params):
        return "error"


nc = NotController()
command = input("Введите команду")
params = input("Введите параметры")
if command == "first":
    print(nc.first_action(params))
elif command == "second":
    print(nc.second_action(params))
elif command == "third":
    print(nc.third_action(params))
else:
    print(nc.error_action(params))
