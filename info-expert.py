import random as rand


class WellWork(object):
    def __init__(self):
        self.oilMass = rand._inst.randint(0, 20)

a = WellWork()
b = WellWork()
c = WellWork()

result = a.oilMass + b.oilMass + c.oilMass
print("%d + %d + %d = %d" % (a.oilMass, b.oilMass, c.oilMass, result))

