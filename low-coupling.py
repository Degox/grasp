class Data:
    def __init__(self, values):
        self.values = values

class FileWriter:
    def write(self, data):
        with open('output.txt', 'r') as f:
            read = f.read()
        with open('output.txt', 'w') as f:
            f.write(read +" "+data)


class Summator:
    def sum(self, list):
        result = 0
        for item in list:
            result += item
        return str(result)


d1 = Data([2, 34, 6, 1, 34, 53])
d2 = Data([4, 6, 21, 5, 3, 45, 2, 2])
summator = Summator()
fw = FileWriter()
fw.write(summator.sum(d1.values))
fw.write(summator.sum(d2.values))
