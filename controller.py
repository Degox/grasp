class Controller(object):
    def main_action(self, command, params):
        if command == "first":
            return self.first_action(params)
        elif command == "second":
            return self.second_action(params)
        elif command == "third":
            return self.third_action(params)
        else:
            return self.error_action(params)

    def first_action(self, params):
        with open('output.txt', 'w') as f:
            f.write(params)
        return "done"

    def second_action(self, params):
        with open('output.txt', 'r') as f:
            return f.read()

    def third_action(self, params):
        return "someText"

    def error_action(self, params):
        return "error"


c = Controller()
input1 = input("Введите команду")
input2 = input("Введите параметры")
print(c.main_action(input1, input2))
