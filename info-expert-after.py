import random as rand


class WellWork(object):
    def __init__(self):
        self.oilMass = rand._inst.randint(0, 20)


# expert
class Wells(object):
    def __init__(self):
        self.wells = []

    def createWell(self):
        self.wells.append(WellWork())
        return self

    def getWellOilMass(self):
        result = 0
        for well in self.wells:
            result += well.oilMass
        return result


w = Wells()
w.createWell().createWell().createWell()

print(w.getWellOilMass())
