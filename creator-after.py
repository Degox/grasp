import random as rand

from abc import ABCMeta, abstractmethod, abstractproperty


# polymorphism
class Well():
    __metaclass__ = ABCMeta

    @abstractmethod
    def getName(self):
        pass


class WellWork(Well):
    def __init__(self):
        self.oilMass = rand._inst.randint(0, 20)

    def getName(self):
        return "WellWork"


class WellRepairWork(Well):
    def __init__(self, work_time):
        self.workTime = work_time

    def getName(self):
        return "WellRepairWork"


class Creator(object):
    def __init__(self):
        self.wwc = WellWorkCreator()
        self.wrc = WellRepairWorkCreator()

    def createWellSomething(self, value=None):
        if value:
            return self.wrc.create(value)
        else:
            return self.wwc.create()


class WellWorkCreator(object):
    def __init__(self):
        self.wells = []

    def create(self):
        work = WellWork()
        self.wells.append(work)
        return work


class WellRepairWorkCreator(object):
    def __init__(self):
        self.repair_works = []

    def create(self, value):
        work = WellRepairWork(value)
        self.repair_works.append(work)
        return work


creator = Creator()
something1 = creator.createWellSomething(12)
something2 = creator.createWellSomething()
print(something1.getName())
print(something2.getName())
