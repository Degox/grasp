class Data:
    def __init__(self, values):
        self.values = values

    def write(self):
        with open('output.txt', 'r') as f:
            read = f.read()
        with open('output.txt', 'w') as f:
            f.write(read + " " + self.sum())

    def sum(self):
        result = 0
        for item in self.values:
            result += item
        return str(result)

d1 = Data([2, 34, 6, 1, 34, 53])
d2 = Data([4, 6, 21, 5, 3, 45, 2, 2])
d1.write()
d2.write()
